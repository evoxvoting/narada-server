var Eth = require('ethjs');
var SignerProvider = require('ethjs-provider-signer');
var sign = require('ethjs-signer').sign;
var lightwallet = require('eth-lightwallet');
var http = require('http');

var contractHash = '0x3ff334887dbf3d8ce5cb4291f71f0b7752605a68';
var abi = [{"constant":true,"inputs":[{"name":"_id","type":"bytes32"},{"name":"voteId","type":"uint256"}],"name":"getPollVote","outputs":[{"name":"deputeeAddress","type":"address"},{"name":"deputeeID","type":"bytes32"},{"name":"deputyName","type":"string"},{"name":"vote","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"questionID","type":"bytes32"},{"name":"deputeeID","type":"bytes32"},{"name":"deputyName","type":"string"},{"name":"questionTitle","type":"string"},{"name":"questionText","type":"string"},{"name":"voteResult","type":"string"}],"name":"vote","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"_id","type":"bytes32"}],"name":"getPoll","outputs":[{"name":"questionID","type":"bytes32"},{"name":"questionTitle","type":"string"},{"name":"questionText","type":"string"},{"name":"votesAmt","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"claimContractOwnership","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"changeContractOwnership","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"pendingContractOwner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"contractOwner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"deputeeAddress","type":"address"},{"indexed":false,"name":"questionID","type":"bytes32"},{"indexed":false,"name":"deputeeID","type":"bytes32"},{"indexed":false,"name":"vote","type":"string"}],"name":"VoteAccepted","type":"event"}]
var password = '123123123';
var RpcUrl = 'https://ropsten.infura.io/vlr9c3QBJDTx6YDpn6QS';

var contracts = {};

var Deputy = require('../models/deputy');

(new Deputy())
    .getAll()
    .then(function (all) {
      all.forEach(function (d) {
        var deputy = {
          _id: d._id,
          address: d.address,
          pKey: d.pKey
        };
        var provider = new SignerProvider(RpcUrl, {
          signTransaction: function(rawTx, cb) { cb(null, sign(rawTx, '0x' + deputy.pKey)) },
          accounts: function(cb) { cb(null, ['0x' + deputy.address]) },
        });
        deputy.eth = new Eth(provider);
        deputy.contractInstance = deputy.eth.contract(abi).at(contractHash);
        contracts[d._id] = deputy;
      })
    });

function EthService() {
  this.createAccount = function (deputyId) {
    return new Promise(function (resolve, reject) {
      var deputy = {};
      lightwallet.keystore.createVault({
        password: password,
      }, function (err, ks) {
        (ks.keyFromPassword(password, function (err, pwDerivedKey) {
          ks.generateNewAddress(pwDerivedKey);
          deputy.address = ks.getAddresses()[0];
          deputy.pKey = ks.exportPrivateKey(deputy.address, pwDerivedKey);
          var provider = new SignerProvider(RpcUrl, {
            signTransaction: function(rawTx, cb) { cb(null, sign(rawTx, '0x' + deputy.pKey)) },
            accounts: function(cb) { cb(null, ['0x' + deputy.address]) },
          });
          deputy.eth = new Eth(provider);
          deputy.contractInstance = deputy.eth.contract(abi).at(contractHash);
          http.get('http://faucet.ropsten.be:3001/donate/'+deputy.address);
          contracts[deputyId] = deputy;
          resolve(contracts[deputyId]);
        }));
      });
    });
  }

  this.vote = function (d) {
    var opt = { from: contracts[d.deputyId].address, value: 0, gas: 4000000 };
    var issueId = Math.abs((d.issueId).hashCode());
    var deputyId = Math.abs((d.deputyId).hashCode());
    return contracts[d.deputyId].contractInstance.vote('0x' + issueId, '0x' + deputyId, d.deputyName, d.issueTitle, d.issueText, d.vote, opt);
  }

  this.getPoll = function (issueId) {
    return contracts[d.deputyId].contractInstance.getPoll(issueId);
  }
}

module.exports = EthService;

String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length === 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};
