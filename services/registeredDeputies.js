var Deputy = require('../models/deputy');

function RegisteredDeputiesService() {
    this.deputies = [];
}

/**
 * Register deputy
 * @param deputyId
 * @returns {*}
 */
RegisteredDeputiesService.prototype.register = function (deputyId, deviceId) {
    var self = this;
    var f = self.deputies.find(function (item) {
        return item._id == deputyId;
    });
    if (f)
        return Promise.resolve();

    (new Deputy()).update({_id: deputyId, deviceId: deviceId, enabled: true});

    return (new Deputy())
        .get(deputyId)
        .then(function (deputy) {
            if (!deputy)
                return Promise.reject('not_found');
            self.deputies.push(deputy);
        });
};

/**
 * Unregister deputy
 * @param deputyId
 * @returns {*}
 */
RegisteredDeputiesService.prototype.unregister = function (deputyId) {
    var self = this;
    for (var i = 0; i < self.deputies.length; i++)
        if (self.deputies[i]._id == deputyId) {
            self.deputies.splice(i, 1);
            return Promise.resolve();
        }

    return Promise.reject('not_found');
};

/**
 * Check if deputy is registered
 * @param deputyId
 * @returns {*}
 */
RegisteredDeputiesService.prototype.isRegistered = function (deputyId) {
    var self = this;
    var f = self.deputies.find(function (item) {
        return item._id == deputyId;
    });
    return Promise.resolve(!!f);
};

module.exports = new RegisteredDeputiesService();
