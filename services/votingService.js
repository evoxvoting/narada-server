var https = require('https');
var Issue = require('../models/issue');
var VotingResult = require('../models/votingResult');
var registeredDeputiesService = require('../services/registeredDeputies');

var EthService = require('../services/ethService');

ethService = new EthService();

function VotingService() {
    this.issue = null;
    this.votes = [];
    this.isOpened = false;
}

/**
 * Initiate voting during 1 minute
 * @param issueId
 */
VotingService.prototype.start = function (issueId) {
    var self = this;

    return (new Issue()).get(issueId)
        .then(function (issue) {
            if (issue) {
                console.info('Voting for issue ' + issueId + ' is started');
                self.isOpened = true;
                self.issue = issue;
            } else
                return Promise.reject({status: 400, error: 'Could not found issue with id: ' + issueId});
        })
        .then(this._sendNotification.bind(this))
        .then(function () {
            self.timer = setTimeout(self._timeoutHandler.bind(self), 28 * 1000);
        });
};

VotingService.prototype.cancel = function (issueId) {
    this.isOpened = false;
    this.votes = [];
    if (this.timer) {
        clearTimeout(this.timer);
        this.timer = null;
    }
};

VotingService.prototype._sendNotification = function () {
    var headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic YTkwNzY5OWMtOTFlZS00ODdiLThhODUtMjgyYWYxNGY2Zjg1"
    };
    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };
    var message = {
        app_id: "30c7a2bb-74fc-410d-b622-ac76dbffa0e8",
        contents: {"en": this.issue.title},
        headings: {"en": "Початок голосування"},
        included_segments: ['All'],
        data: {issue_id: this.issue._id, issue_title: this.issue.title}
    };
    if (this.issue.group && this.issue.group !== 'All')
      message.filters = [
        {"field": "tag", "key": this.issue.group, "relation": "exists"}
      ]
    return new Promise(function (resolve, reject) {
        var body = [];
        var req = https.request(options, function (res) {
            res.on('data', function (data) {
                body.push(data.toString());
            });
            res.on('end', function () {
                console.log(body.join(''));
                resolve();
            });
        });

        req.on('error', function (e) {
            console.log("ERROR:");
            console.log(e);
            reject(e);
        });

        req.write(JSON.stringify(message));
        req.end();
    });
};

/**
 * Add vote to current voting
 * @param deputyId
 * @param vote
 * @returns {*}
 */
VotingService.prototype.addVote = function (issueId, deputyId, vote) {
    if (this.isOpened && this.issue._id == issueId && registeredDeputiesService.isRegistered(deputyId)) {
        if (!this.votes.find(function (item) {
                return item.deputy_id == deputyId;
            })) {
            console.info('Vote for issue ' + issueId + ' is registered');
            this.votes.push({
                _id: this.issue._id + '.' + deputyId,
                issue_id: this.issue._id,
                deputy_id: deputyId,
                vote: vote
            });

            var d = registeredDeputiesService.deputies.find(function (item) {
                return item._id == deputyId;
            });

            (new Issue())
                .get(this.issue._id)
                .then(function (issue) {
                  // console.log(issue)
                  ethService.vote({
                    deputyId: deputyId,
                    issueId: issueId,
                    deputyName: d.name,
                    issueTitle: issue.title,
                    issueText: issue.description,
                    vote: vote
                  }).then(function (r) {
                    console.log('votehash: ', r);
                  }).catch(err => {
                    console.log(err)
                  })
                });

            return (registeredDeputiesService.deputies.length == this.votes.length) ?
                this.complete() :
                Promise.resolve();
        }
    }
    return Promise.reject('aborted');
};

VotingService.prototype._timeoutHandler = function () {
    this.complete()
        .catch(console.error);
    this.timer = null;
};

VotingService.prototype.complete = function () {
    var self = this;
    this.isOpened = false;
    console.info('Voting for issue ' + this.issue._id + ' is finished');

    this.issue.vote_date = (new Date()).toISOString();

    return Promise.all([
        (new Issue())
            .update(this.issue),

        (new VotingResult())
            .deleteBySessionIssueId(this.issue._id)
            .then(function () {
                if (self.votes.length > 0)
                    return (new VotingResult())
                        .insertMany(self.votes);
            })]);
};

module.exports = VotingService;
