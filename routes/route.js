var express = require('express');
var router = express.Router();
var sessionIssue = require('./issue.route');
var voting = require('./voting.route');
var deputy = require('./deputy.router');

router.use(function (req, res, next) {
    res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET, OPTIONS, DELETE",
        "Access-Control-Allow-Headers": "app-mode, content-type, auth-token",
        "Access-Control-Expose-Headers": "X-Total-Results"
    });
    next();
});

router.use('/issue', sessionIssue);
router.use('/voting', voting);
router.use('/deputy', deputy);

module.exports = router;