var express = require('express');
var router = express.Router();
var Deputy = require('../models/deputy');
var RegisteredDeputiesService = require('../services/registeredDeputies');

var EthService = require('../services/ethService');

ethService = new EthService();

var registeredDeputies = RegisteredDeputiesService;

/* GET all deputies */
router.get('/', function(req, res) {
    (new Deputy)
        .getAll()
        .then(res.json.bind(res))
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* CREATE deputy */
router.post('/', function(req, res) {
    (new Deputy())
        .put(req.body, (e, r) => {
          ethService.createAccount(r.insertedId).then(function (eth) {
            (new Deputy()).update({_id: r.insertedId, address: eth.address, pKey: eth.pKey});
          });
        })
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* UPDATE deputy */
router.post('/:deputy_id', function(req, res) {
    (new Deputy())
        .update(req.body)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* GET all registered deputies */
router.get('/registered', function(req, res) {
    res.json(registeredDeputies.deputies);
});

/* POST register deputy */
router.post('/:deputyId/register/:deviceId', function (req, res) {
    registeredDeputies.register(req.params.deputyId, req.params.deviceId)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* POST unregister deputy */
router.post('/:deputyId/unregister', function (req, res) {
    registeredDeputies.unregister(req.params.deputyId)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            if (err == 'not_found')
                res.status(404).end();
            else
                res.status(500).send(err);
        });
});


/* GET check if deputy is registered */
router.get('/:deputyId/register', function (req, res) {
    registeredDeputies.isRegistered(req.params.deputyId)
        .then(function (state) {
            res.json({state: state});
        });
});

/* DELETE deputy */
router.delete('/:deputyId', function(req, res) {
    (new Deputy())
        .remove(req.params.deputyId)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

module.exports = router;
