var express = require('express');
var router = express.Router();
var VotingService = require('../services/votingService');
var VotingResult = require('../models/votingResult');
var Issue = require('../models/issue');

var initiatedVoting;

/* Start voting */
router.post('/:issue_id', function (req, res) {
    if (initiatedVoting && initiatedVoting.isOpened) {
        console.error('Can not start voting.');
        res.status(400).end();
        return;
    }

    initiatedVoting = new VotingService();
    initiatedVoting.start(req.params.issue_id)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            console.error(err);
            if (err.status == 400)
                res.status(400).send(err);
            else
                res.status(500).send(err);
        });
});

router.delete('/:issue_id', function (req, res) {
    if (initiatedVoting && initiatedVoting.isOpened) {
        initiatedVoting.cancel(req.params.issue_id);
    }
    res.end();
});

router.get('/:issue_id/status', function (req, res) {
    if (initiatedVoting && initiatedVoting.issue && initiatedVoting.issue._id == req.params.issue_id) {
        res.json({
            voting: initiatedVoting.isOpen,
            votes: initiatedVoting.votes
        });
    } else
        res.end();
});

/* Get voting results */
router.get('/:issue_id', function (req, res) {

    Promise.all([(new Issue())
        .get(req.params.issue_id),
        (new VotingResult())
            .getBySessionIssueId(req.params.issue_id)])

        .then(function (data) {
            res.json({
                _id: req.params.issue_id,
                vote_date: data[0].vote_date,
                votes: data[1]
            });
        })
        .catch(function (err) {
            console.error(err);
            res.status(500).send(err);
        });
});

/* Add vote */
router.post('/:issue_id/vote', function (req, res) {
    initiatedVoting
        .addVote(req.params.issue_id, req.body.deputy_id, req.body.vote)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            console.error(err);
            if (err == 'aborted')
                res.status(400).end();
            else
                res.status(500).send(err);
        });
});

module.exports = router;
