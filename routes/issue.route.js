var express = require('express');
var router = express.Router();
var Issue = require('../models/issue');

/* GET all issues */
router.get('/', function(req, res) {
    (new Issue())
        .getAll()
        .then(res.json.bind(res))
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* GET issue info */
router.get('/:issue_id', function(req, res) {
    (new Issue())
        .get(req.params.issue_id)
        .then(res.json.bind(res))
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* POST add a new issue */
router.post('/', function(req, res) {
    (new Issue())
        .put(req.body)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* DELETE issue */
router.delete('/:issue_id', function(req, res) {
    (new Issue())
        .remove(req.params.issue_id)
        .then(function () {
            res.end();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

module.exports = router;
