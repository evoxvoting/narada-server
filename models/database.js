var Promise = require("bluebird");
var MongoClient = require('mongodb').MongoClient;

var MONGO_CONNECTION_STRING = 'mongodb://localhost:27017/narada';

function getConnection() {
    return MongoClient.connect(MONGO_CONNECTION_STRING, { promiseLibrary: Promise });
}

function closeConnection(db) {
    return db.close();
}

function createDatabase() {
    getConnection()
        .then(function (db) {
            return createCollections(db).then(db.close.bind(db));
        })
        .catch(console.error);
}

function createCollections(db) {
    return Promise.all([
        createCollection(db, 'deputies'),
        createCollection(db, 'issues'),
        createCollection(db, 'voting_results')
    ]);
}

function createCollection(db, name) {
    return db.createCollection(name, {autoIndexId: true});
}


module.exports = {
    getConnection: getConnection,
    closeConnection: closeConnection,
    createDatabase: createDatabase
};