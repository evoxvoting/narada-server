var Model = require('./model');

function Issue() {
    Model.apply(this, ['issues']);
}

Issue.prototype = Object.create(Model.prototype);
Issue.prototype.constructor = Model;

module.exports = Issue;