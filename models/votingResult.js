var Model = require('./model');

function VotingResult() {
    Model.apply(this, ['voting_results']);
}

VotingResult.prototype = Object.create(Model.prototype);
VotingResult.prototype.constructor = Model;

VotingResult.prototype.deleteBySessionIssueId = function (issueId) {
    var self = this;
    return this.database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .deleteMany({issue_id: issueId})
                .finally(db.close.bind(db));
        })
};

VotingResult.prototype.getBySessionIssueId = function (issueId) {
    var self = this;
    return this.database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .find({issue_id: issueId})
                .toArray()
                .finally(db.close.bind(db));
        })
};

VotingResult.prototype.insertMany = function (data) {
    var self = this;
    return this.database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .insertMany(data)
                .finally(db.close.bind(db));
        })
};

module.exports = VotingResult;