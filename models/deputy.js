var Model = require('./model');

function Deputy() {
    Model.apply(this, ['deputies']);
}

Deputy.prototype = Object.create(Model.prototype);
Deputy.prototype.constructor = Model;

module.exports = Deputy;