var database = require('./database');

function Model(collectionName) {
    this.collectionName = collectionName;
    this.database = database;
}

Model.prototype.getAll = function () {
    var self = this;
    return database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .find()
                .toArray()
                .finally(db.close.bind(db));
        });
};

Model.prototype.get = function (id) {
    var self = this;
    return database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .find({_id: id})
                .limit(1)
                .next()
                .finally(db.close.bind(db));
        });
};

Model.prototype.put = function (doc, cb) {
    var self = this;
    return database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .insertOne(doc, cb)
                .finally(db.close.bind(db));
        });
};

Model.prototype.update = function (doc) {
    var self = this;
    return database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .updateOne({_id: doc._id}, { $set: doc}, { upsert: true })
                .finally(db.close.bind(db));
        });
};

Model.prototype.remove = function (id) {
    var self = this;
    return database.getConnection()
        .then(function (db) {
            return db
                .collection(self.collectionName)
                .remove({_id: id})
                .finally(db.close.bind(db));
        });
};

module.exports = Model;
