var express = require('express');
var bodyParser = require('body-parser');
var router = require('./routes/route');
var database = require('./models/database');

database.createDatabase();
var app = express();
app.use(bodyParser.json());

app.use('/', router);

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});